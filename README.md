# Computer Vision: Models, Learning, and Inference

<br>

![Computer Vision Models Learning Inference](https://gitlab.com/jadro-ai-public/computer-vision-models-learning-inference/-/raw/master/book_cover.jpg)

<br>

| Name | Description |
| ------ | ------ |
| CVMLI folder | MATLAB code |
| CV - MLI - Stefan Stavrev.pdf | Algorithm explanations and visualizations |

<br>

I implemented many ML algorithms from the book "Computer Vision: Models, Learning, and Inference"<br>
under supervision by the author Simon J. D. Prince.

My work is also available on the official website of the book [www.computervisionmodels.com](http://www.computervisionmodels.com/).

[Amazon Link](https://www.amazon.com/Computer-Vision-Models-Learning-Inference/dp/1107011795/ref=sr_1_1?s=books&ie=UTF8&qid=1334662414&sr=1-1)